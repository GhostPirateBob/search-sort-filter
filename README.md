# loaded search sort filter

### Project setup
```
yarn install
```

#### Compiles and hot-reloads for development
```
yarn serve
```

#### Compiles and minifies for production
```
yarn build
```


#### Compiles and minifies for production (watch mode)
```
yarn build-watch
```

#### SCSS Bugfix (should fire automatically)
```
yarn postinstall

```

### Lazy Imge Container code

```
v-lazy-container="{ 
  selector: '.card-img-top', 
  error: $root.$data.apiEndpointApartments + 'wp-content/themes/mystirling-understrap/img/error.png', 
  loading: listing.primary_image_xs
}"
```

## Handy links/ideas

### Vue List Rendering

  https://vuejs.org/v2/guide/list.html

###  Vue List Transitions

  https://vuejs.org/v2/guide/transitions.html

###  Lodash Filter Function

  https://lodash.com/docs/4.17.15#filter

###  List Item Transitions

  https://vuejs.org/v2/guide/transitions.html
  https://codepen.io/ktsn/pen/vJgYxB

###  V-Lazy-Image

  https://github.com/alexjoverm/v-lazy-image

###  Vue-Virtual-Scoller

  https://github.com/Akryum/vue-virtual-scroller
    
  (based on) http://googlechromelabs.github.io/ui-element-samples/infinite-scroller/

###  Vue-Simple-Suggest

  https://github.com/KazanExpress/vue-simple-suggest

  (based on ) http://twitter.github.io/typeahead.js/examples/

###  Vue-Content-Loader

  https://github.com/egoist/vue-content-loader


