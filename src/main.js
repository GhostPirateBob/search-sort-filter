import Vue from 'vue'
import App from './App.vue'
import router from './router'
import _ from 'lodash'
import * as mdc from 'material-components-web';
import vSelect from 'vue-select'
import VueLazyload from 'vue-lazyload'

const baseURL = "https://mystirling.com/";
const themeName = 'mystirling';

export default themeName;
export { MDCCheckbox } from '@material/checkbox';
export { MDCChipSet } from '@material/chips';
export { MDCDialog } from '@material/dialog';
export { MDCDrawer } from '@material/drawer';
export { MDCFormField } from '@material/form-field';
export { MDCIconButtonToggle } from '@material/icon-button';
export { MDCLinearProgress } from '@material/linear-progress';
export { MDCList } from '@material/list';
export { MDCMenu } from '@material/menu';
export { MDCRadio } from '@material/radio';
export { MDCRipple } from '@material/ripple/index';
export { MDCSelect } from '@material/select';
export { MDCSlider } from '@material/slider';
export { MDCSnackbar } from '@material/snackbar';
export { MDCSwitch } from '@material/switch';
export { MDCTabBar } from '@material/tab-bar';
export { MDCTextField } from '@material/textfield';
export { MDCTextFieldHelperText } from '@material/textfield/helper-text';
export { MDCTopAppBar } from '@material/top-app-bar';
export { MDCDataTable } from '@material/data-table';

Vue.use(VueLazyload)

Vue.component('v-select', vSelect)

// Configure whether to allow vue-devtools inspection
Vue.config.devtools = true

// Enable component init, compile, render and patch performance tracing in the browser devtool timeline.
Vue.config.performance = true

// Prevent the production tip on Vue startup.
Vue.config.productionTip = true

// Suppress all Vue logs and warnings
Vue.config.silent = false

// Define custom key alias(es) for v-on.
Vue.config.keyCodes = { 
  v: 86,
  f1: 112,
  // camelCase won`t work
  mediaPlayPause: 179,
  // instead you can use kebab-case with double quotation marks
  "media-play-pause": 179,
  up: [38, 87]
 }

new Vue({ 
  router,
  data: function () {
    return {
      apiEndpointApartments: "http://mystirling.com/",
      apiEndpointDiscounts: "http://amawa.lcprojects.com.au/",
      discounts: [],
      discountCategories: [],
      developments: [],
      listings: [],
      priceMinSelected: null,
      priceMaxSelected: null,
      bedroomsSelected: null,
      developmentSelected: null,
      sortingOptions: [ 
        { item: ['price_match'], order: ['asc'], label: "Price (Lowest - Highest)", code: "price_match-asc" },
        { item: ['price_match'], order: ['desc'], label: "Price (Highest - Lowest)", code: "price_match-desc" },
        { item: ['attributes_bedrooms'], order: ['asc'], label: "Bedrooms (Lowest - Highest)", code: "bedrooms-asc" },
        { item: ['attributes_bedrooms'], order: ['desc'], label: "Bedrooms (Highest - Lowest)", code: "bedrooms-desc" }
      ],
      sortingSelected: null,
      currentSortOrder: ['random'],
      priceMinArray: [ 
        { label: "$200,000", code: "200000" },
        { label: "$250,000", code: "250000" },
        { label: "$300,000", code: "300000" },
        { label: "$350,000", code: "350000" },
        { label: "$400,000", code: "400000" },
        { label: "$450,000", code: "450000" },
        { label: "$500,000", code: "500000" },
        { label: "$550,000", code: "550000" },
        { label: "$600,000", code: "600000" },
        { label: "$650,000", code: "650000" },
        { label: "$700,000", code: "700000" },
        { label: "$750,000", code: "750000" },
        { label: "$800,000", code: "800000" },
        { label: "$850,000", code: "850000" },
        { label: "$900,000", code: "900000" },
        { label: "$950,000", code: "950000" },
        { label: "$1,000,000", code: "1000000" }
      ],
      priceMaxArray: [ 
        { label: "$200,000", code: "200000" },
        { label: "$250,000", code: "250000" },
        { label: "$300,000", code: "300000" },
        { label: "$350,000", code: "350000" },
        { label: "$400,000", code: "400000" },
        { label: "$450,000", code: "450000" },
        { label: "$500,000", code: "500000" },
        { label: "$550,000", code: "550000" },
        { label: "$600,000", code: "600000" },
        { label: "$650,000", code: "650000" },
        { label: "$700,000", code: "700000" },
        { label: "$750,000", code: "750000" },
        { label: "$800,000", code: "800000" },
        { label: "$850,000", code: "850000" },
        { label: "$900,000", code: "900000" },
        { label: "$950,000", code: "950000" },
        { label: "$1,000,000", code: "1000000" },
        { label: "$1,500,000", code: "1500000" },
        { label: "$2,000,000", code: "2000000" },
        { label: "$2,250,000", code: "2250000" },
        { label: "$2,500,000", code: "2500000" },
        { label: "$2,750,000", code: "2750000" },
        { label: "$3,000,000", code: "3000000" }
      ],
      bedroomsArray: [ 
        { label: "1 Bedroom", code: "1" },
        { label: "2 Bedrooms", code: "2" },
        { label: "3 Bedrooms", code: "3" }
      ]
    }
  },
  render: h => h(App)
}).$mount('#app');
