import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/Home.vue'
import Apartments from './components/ApartmentListings.vue'
import ApartmentSearchBox from './components/ApartmentSearchBox.vue'
import SingleMemberBenefits from './components/SingleMemberBenefits.vue'

import _ from "lodash"

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/single-member-benefits',
      name: 'single-member-benefits',
      component: SingleMemberBenefits
    },
    {
      path: '/apartment-search-box',
      name: 'search',
      component: ApartmentSearchBox
    },
    {
      path: '/apartments',
      name: 'apartments',
      component: Apartments
    }
  ]
})
