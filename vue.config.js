const path = require( 'path' )
const fs = require( 'fs' )

module.exports = {
  configureWebpack: {
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'app.js',
      chunkFilename: 'app-chunk.js'
    }
  },
  publicPath: '/',
  css: {
    loaderOptions: {
      sass: {
        includePaths: [
          './node_modules',
          './node_modules/@material',
          './node_modules/vue-select/src/scss'
        ]
      }
    },
    extract: {
      filename: 'app.css',
      chunkFilename: 'app-chunk.css',
    },
  },
  devServer: {
    host: '0.0.0.0',
    hot: true,
    port: 3000,
    compress: false,
    allowedHosts: [
      'loadeddev.online',
      'lcprojects.com.au',
      'loadedcloud.com.au',
      'apps.loadedcloud.com.au',
      'loadedcommunications.com.au',
      'local',
      '203.59.97.206',
      '192.168.1.1',
      '192.168.1.2',
      '192.168.1.3',
      '192.168.1.10',
      '192.168.1.11',
      '192.168.1.12',
      '192.168.1.13',
      '192.168.1.14',
      '192.168.1.15',
      '*'
    ]
  }
};
